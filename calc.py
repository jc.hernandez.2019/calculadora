def suma(num1, num2):
    return num1 + num2


def resta(num1, num2):
    return num1 - num2


suma1 = suma(1, 2)
print("La suma de 1 y 2 (1+2) es", suma1)

suma2 = suma(3, 4)
print("La suma de 3 y 4 (3+4) es", suma2)

resta1 = resta(6, 5)
print("La resta 5 de 6 (6-5) es", resta1)

resta2 = resta(8, 7)
print("La resta 7 de 8 (8-7) es", resta2)
